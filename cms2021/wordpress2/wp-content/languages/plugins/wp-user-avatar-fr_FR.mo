��    K      t  e   �      `  1   a     �  	   �     �     �     �     �     �     �           )  
   0     ;  +   H     t     �     �  �   �     !     /     6     L  
   R     ]     d     u     {     �     �     �  1   �     �  
   �     	     	     (	     6	     G	     T	     [	     h	     �	     �	     �	  	   �	     �	     �	  <   �	  .   �	  	   $
     .
     5
  N   9
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
          '     <     S     g  $   �     �     �  "   �     �          '    ;  +   �     �  
          &   $     K     P     Y     a  9   s     �     �     �  4   �          1     :  �   P     �     �     �               )     2     O  	   U     _  ,   g     �  D   �     �  
   �        )        9     L     `     m     u  /   �     �  	   �  
   �     �  	   �     �  R      ?   S  	   �     �     �  g   �               5     ;     O     c     k     �     �     �     �     �     �       $   .     S     g  "   �     �     �     �         $          H   ,   @   "               9      ;   J   	       A   5   /   >      .   
   1         D   ?   6   2   :                                                       +   )      -   8   '      &       C   %      (   F                 4       G      =                  !   0   3   K          B           E       #   7      <               *             I            %s exceeds the maximum upload size for this site. <code>jpg jpeg png gif</code> Alignment Allowed Files Automatically add paragraphs Blank Caption Center Choose Image Crop avatars to exact dimensions Custom Custom URL Description: Disable Gravatar and use only local avatars Dismiss this notice Edit Footer Text For users without a custom avatar of their own, you can either display a generic logo or a generated one based on their e-mail address. Gravatar Logo Height Identicon (Generated) Image Image File Insert Insert into Post Large Left Link To Maximum upload file size: %d%s. Medium Memory exceeded. Please try another smaller file. MonsterID (Generated) My Profile Mystery Man Open link in a new window Original Size Profile updated. ProfilePress Remove Remove Image Resize avatars on upload Retro (Generated) Right Search Select %s Settings Size Sorry, this file type is not permitted for security reasons. This file is not an image. Please try another. Thumbnail Title: URL Unable to create directory %s. Is its parent directory writable by the server? Undo Update Profile Upload User Name Wavatar (Generated) Width You are already logged in. You are already registered. [avatar_upload] https://profilepress.net wp_user_avatar_align wp_user_avatar_caption wp_user_avatar_link wp_user_avatar_link_external wp_user_avatar_link_external_section wp_user_avatar_size wp_user_avatar_size_number wp_user_avatar_size_number_section wp_user_avatar_target wp_user_avatar_upload wp_user_avatar_user PO-Revision-Date: 2021-11-24 07:16:12+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - User Registration, Login Form, User Profile &amp; Membership – ProfilePress (Formerly WP User Avatar) - Stable (latest release)
 %s dépasse la limite d’envoi de ce site. <code>jpg jpeg png gif</code> Alignement Fichiers autorisés Créer automatiquement les paragraphes Vide Légende Centré Choisir une image Recadrer les avatars pour parvenir aux dimensions exactes Personnalisé URL personnalisée Description&nbsp;: Désactiver Gravatar et utiliser les avatars fournis Ignorer cette notification Modifier Texte de pied de page Les utilisateurs n’ayant pas d’avatar peuvent se voir attribuer un logo générique, ou un avatar généré à partir de leur adresse de messagerie. Logo Gravatar Hauteur Identicon (généré) Image Fichier image Insérer Insérer dans la publication Grand À gauche Lier à Taille maximum de fichier autorisée : %d%s. Moyen Mémoire dépassée. Veuillez réessayer avec un fichier plus petit. MonsterID (généré) Mon Profil Homme mystère Ouvrir le lien dans une nouvelle fenêtre Taille d’origine Profil mis à jour. ProfilePress Retirer Retirer l’image Redimensionner avatars lors du téléchargement Retro (généré) À droite Rechercher Sélectionner %s Réglages Taille Désolé, ce type de fichier n’est pas autorisé pour des raisons de sécurité. Ce fichier n’est pas une image. Veuillez en envoyer un autre. Miniature Titre : URL Impossible de créer le dossier %s. Son dossier parent est-il accessible en écriture par le serveur ? Annuler Mettre à jour votre profil Envoi Nom d’utilisateur Wavatar (généré) Largeur Vous êtes déjà connecté. Vous êtes déjà enregistré. [avatar_upload] https://profilepress.net/ wp_user_avatar_align wp_user_avatar_caption wp_user_avatar_link wp_user_avatar_link_external wp_user_avatar_link_external_section wp_user_avatar_size wp_user_avatar_size_number wp_user_avatar_size_number_section wp_user_avatar_target wp_user_avatar_upload wp_user_avatar_user 