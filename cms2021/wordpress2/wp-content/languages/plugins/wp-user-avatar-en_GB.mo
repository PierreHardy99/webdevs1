��    E      D  a   l      �  1   �     #  	   A     K     Y     v     |     �     �      �     �  
   �     �  +   �            �        �     �     �     �  
   �     �     �     �     �               ,  1   3     e     {     �     �     �     �     �     �     �     �     	     	  	   	     #	     ,	  .   1	  	   `	     j	     q	  N   u	     �	     �	     �	  	   �	     �	     �	     
     
     (
     ?
     S
  $   p
     �
     �
  "   �
     �
     �
       �  '  1   �     �  	   �               0     6     >     E      R     s  
   z     �  +   �     �     �  �   �     _     m     t     �  
   �     �     �     �     �     �     �     �  1   �          5     A     [     i     z     �     �     �     �     �     �  	   �     �     �  .   �  	        $     +  N   /     ~     �     �  	   �     �     �     �     �     �     �       $   *     O     c  "   ~     �     �     �     8   $           E                            )          6   7         &      4                 .   D   -   >   #          C       :          <   =       ?            (   @      /       5         A   *       	   !              "   9          '          0                                %      1          ,      3   
       ;   +       B       2              %s exceeds the maximum upload size for this site. <code>jpg jpeg png gif</code> Alignment Allowed Files Automatically add paragraphs Blank Caption Center Choose Image Crop avatars to exact dimensions Custom Custom URL Description: Disable Gravatar and use only local avatars Dismiss this notice Edit For users without a custom avatar of their own, you can either display a generic logo or a generated one based on their e-mail address. Gravatar Logo Height Identicon (Generated) Image Image File Insert Insert into Post Large Left Link To Maximum upload file size: %d%s. Medium Memory exceeded. Please try another smaller file. MonsterID (Generated) Mystery Man Open link in a new window Original Size Profile updated. ProfilePress Remove Remove Image Resize avatars on upload Retro (Generated) Right Search Select %s Settings Size This file is not an image. Please try another. Thumbnail Title: URL Unable to create directory %s. Is its parent directory writable by the server? Undo Update Profile Upload User Name Wavatar (Generated) Width [avatar_upload] wp_user_avatar_align wp_user_avatar_caption wp_user_avatar_link wp_user_avatar_link_external wp_user_avatar_link_external_section wp_user_avatar_size wp_user_avatar_size_number wp_user_avatar_size_number_section wp_user_avatar_target wp_user_avatar_upload wp_user_avatar_user PO-Revision-Date: 2020-04-30 11:46:41+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: en_GB
Project-Id-Version: Plugins - User Registration, Login Form, User Profile &amp; Membership – ProfilePress (Formerly WP User Avatar) - Stable (latest release)
 %s exceeds the maximum upload size for this site. <code>jpg jpeg png gif</code> Alignment Allowed Files Automatically add paragraphs Blank Caption Centre Choose Image Crop avatars to exact dimensions Custom Custom URL Description: Disable Gravatar and use only local avatars Dismiss this notice Edit For users without a custom avatar of their own, you can either display a generic logo or a generated one based on their e-mail address. Gravatar Logo Height Identicon (Generated) Image Image File Insert Insert into Post Large Left Link To Maximum upload file size: %d%s. Medium Memory exceeded. Please try another smaller file. MonsterID (Generated) Mystery Man Open link in a new window Original Size Profile updated. ProfilePress Remove Remove Image Resize avatars on upload Retro (Generated) Right Search Select %s Settings Size This file is not an image. Please try another. Thumbnail Title: URL Unable to create directory %s. Is its parent directory writable by the server? Undo Update Profile Upload User Name Wavatar (Generated) Width [avatar_upload] wp_user_avatar_align wp_user_avatar_caption wp_user_avatar_link wp_user_avatar_link_external wp_user_avatar_link_external_section wp_user_avatar_size wp_user_avatar_size_number wp_user_avatar_size_number_section wp_user_avatar_target wp_user_avatar_upload wp_user_avatar_user 